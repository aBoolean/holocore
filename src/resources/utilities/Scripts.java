/***********************************************************************************
* Copyright (c) 2015 /// Project SWG /// www.projectswg.com                        *
*                                                                                  *
* ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on           *
* July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies.  *
* Our goal is to create an emulator which will provide a server for players to     *
* continue playing a game similar to the one they used to play. We are basing      *
* it on the final publish of the game prior to end-game events.                    *
*                                                                                  *
* This file is part of Holocore.                                                   *
*                                                                                  *
* -------------------------------------------------------------------------------- *
*                                                                                  *
* Holocore is free software: you can redistribute it and/or modify                 *
* it under the terms of the GNU Affero General Public License as                   *
* published by the Free Software Foundation, either version 3 of the               *
* License, or (at your option) any later version.                                  *
*                                                                                  *
* Holocore is distributed in the hope that it will be useful,                      *
* but WITHOUT ANY WARRANTY; without even the implied warranty of                   *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                    *
* GNU Affero General Public License for more details.                              *
*                                                                                  *
* You should have received a copy of the GNU Affero General Public License         *
* along with Holocore.  If not, see <http://www.gnu.org/licenses/>.                *
*                                                                                  *
***********************************************************************************/
package resources.utilities;

import java.io.File;

import org.python.core.Py;
import org.python.util.PythonInterpreter;

public final class Scripts {
	private static final String SCRIPTS_PATH = "scripts/";
	
	// TODO: Variable arguments?
	
	public static void execute(String script, String method) {
		if (!scriptExists(script))
			return;
		
		PythonInterpreter interp = new PythonInterpreter();
		interp.execfile(SCRIPTS_PATH + script);
		interp.get(method).__call__();
	}
	
	public static void execute(String script, String method, Object arg1) {
		if (!scriptExists(script))
			return;
		
		PythonInterpreter interp = new PythonInterpreter();
		interp.execfile(SCRIPTS_PATH + script);
		interp.get(method).__call__(Py.java2py(arg1));
	}
	
	public static void execute(String script, String method, Object arg1, Object arg2) {
		if (!scriptExists(script))
			return;
		
		PythonInterpreter interp = new PythonInterpreter();
		interp.execfile(SCRIPTS_PATH + script);
		interp.get(method).__call__(Py.java2py(arg1), Py.java2py(arg2));
	}
	
	public static void execute(String script, String method, Object arg1, Object arg2, Object arg3) {
		if (!scriptExists(script))
			return;
		
		PythonInterpreter interp = new PythonInterpreter();
		interp.execfile(SCRIPTS_PATH + script);
		interp.get(method).__call__(Py.java2py(arg1), Py.java2py(arg2), Py.java2py(arg3));
	}
	
	public static void execute(String script, String method, Object arg1, Object arg2, Object arg3, Object arg4) {
		if (!scriptExists(script))
			return;
		
		PythonInterpreter interp = new PythonInterpreter();
		interp.execfile(SCRIPTS_PATH + script);
		interp.get(method).__call__(Py.java2py(arg1), Py.java2py(arg2), Py.java2py(arg3), Py.java2py(arg4));
	}
	
	private static boolean scriptExists(String file) {
		return new File(SCRIPTS_PATH + file).exists();
	}
}
